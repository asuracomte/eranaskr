# Path A: Daughter of Lucifer

## Stage 1: Getting to know your local devil mommy
- Make it so the MC (and player) trusts Shinki.
- At this stage, Shinki is unfamiliar with the player and so are you.
- Shinki will help the player feel like she's trustworthy by giving her the mini Shinki.
- Quests here will be remedial, mundane, and otherwise vanilla in terms of fetishes

### Quests:
#### 1: 

## Stage 2: Goodnight Potty
- Make MC fully diaper dependent.

### Training Stuff?
If you want ideas for training it can be at 3 different angles

- mental

- physical

- stimulation

Mental would be new mental desires and conditioning, generally people encouraging behavior (say being a dolly, baby, dependent, helpless, attention span, reasoning, maturity, IQ

physical would be speech, physical ability (walking to crawling), hand eye coordination (eating for herself but with her hands and making a mess or unable to open doors or do things), potty training loss

stimulation would be world sounds/activity or conditional wording an example of those would be 

emotional calm through paci (kiara cant calm down without her paci now)

headpats and being conditioned to attach to people who give her affection

bells or sounds through the daycare that encourage behavior an example of this would be when being changed a bell next to the changing table lets other kids know she needs a change, and as other kids hear the bell they are conditioned to use their diapers, or even the changing room is not always accessible due to this or even the potty as all of the kids try and be the ones in the potty

(basicly when the bell goes off everyone is going to the potty and it will be a loooooooong time of helpless scenes of going to pee and needing a change only to hear the bell again~)

breast milk dependency would be another for a little who is fed more and more breast milk eventually they will stop asking for normal food and ID the breast milk as food so when she is hungry her mind goes "ask mommy for milkies" (this would be happening with low maturity and IQ as she isnt thinking clearly anymore)

my idea of this would be that kiara is at the daycare for an entire week or month and depending on how they resisted the daycare will have new endings as a means of doing things and if you prove your old enough to be a big girl you stay as an adult (tho no one wants that~ =p)

### Wetting Ideas
- How about environmental hazards such as flower pollen outside, dust inside, and spilled pepper or baby powder in the kitchen or nursery that can cause Kiera too sneeze. Depending on her traits and potty training this can causer to her have an accident. If her PT is high then she has to be on her final warning to loose control but if it's 25 or lower then she can have an accident even without warnings. Sneeze Hazards could also train her body to have an accident everytime she sneezes or have other effects.
- Wetting when Kiera sees someone she LIKE likes, wetting when excited (ie, levelling up).. any more ideas? 🤔
- Wetting on the slide or swing from being in the moment and forgetting about adult things
- How about the classic Pavlov's Bell? Like, Kiera wets/messes whenever she hears the sound of a bell? 
- Wetting when asked if she has to go potty..
- Maybe feeling the need to go as soon as she's back in a new diaper?
- Here's an idea. Kiera can become clumsy, and if she falls onto her padding, she can wet/mess herself without knowing.
- Also, speaking of unintentional messing, is there a scene where Kiera can mess herself without realizing or giving it much mind? For example, in Pants' Labyrinth, if your potty and maturity stat are low enough, it will cause your PC to just squat and let go on instinct, not realizing that the strange odor they are smelling is coming from their diaper. Would you be interested in doing something similar

### 1.5.1A Transition
- Shinki gives a cup of Celestial Peach juice, with an incontinence chip inside of it.
- It gives the MC a couple-hundred max STA and ENE, but also has a little surprise that'll kick in 3 days.

### 2.0 Like Clockwork
- After the third day, the MC will wake up to one of three things:
    - MC will have wet her bed (not padded).
    - MC will have wet her diaper (if padded).
    - MC will have wet her diaper a lot harder than usual (if padded and is a bedwetter (3 accidents within the last 14 days)).
- This will repeat every 3 days.

- MC will realize that something is wrong, that she had either wet the bed, or has produced much more piss than usual in her sleep.
- After the third bedwetting due to the incontinence chip, Shinki will continue her plan.

- Going to Eirin will detect the incontinence chip (an abnormal implant in your urinary system). You can get this removed but it'll make Shinki mad.
- Going to Shinki will tell you it's a little side-effect of the juice. She will suggest that you wear some pads in-case it happens.
- You can accept that you are unable to control your bladder at night and ask for a bedwetter pad (higher quality play mat)

### 2.0.1A Unexpected Urgency
- OAB part of the incontinence chip activates.
- She will have random OAB flare-ups, causing her to become extremely desperate and can cause an accident.
- First event will have the MC get an OAB flare-up, try to find a toilet but end up flooding her pamps

- Going to Eirin will remove the chip, but will have a slight complication (-1 bladder training)
- Going to Shinki will tell you that you'll need to get pads for daytime as well, in case you are unable to make it to a toilet.

### 2.0.2A I can hold it during the day, right?
- Daytime part of the incontinence chip fully activates.
- MC will notice that she is more desperate than usual when holding.
- If scat is on, move to 2.0.3A, else move to 2.1A.

- Going to Eirin will remove the chip, but will have a slight complication (-2 bladder training)
- Going to Shinki will result in her putting you into a pull-up and says to try to get to the toilet as many times as you can.

### 2.0.3A Blort!
- IBS part of the incontinence chip activates.
- First event will have the MC get an IBS flare-up, attempt to hold it, but end up filling her pamps with diarrhea

- Going to Eirin will remove the chip, but will have a slight complication (-3 bladder and bowel training)
- Going to Shinki will result in her diapering you. She says that a bowel event like that needs to be contained within proper protection.
- Shinki will destroy your underwear during after this point if talked to

### 2.0.4A It's a habit now...
- Daytime bowel part of the incontinence chip acivates.
- MC will have an unexpected bowel movement inside her diaper, but has the consistency of a regular poop (and not diarrhea).

- The chip can still be removed by Eirin, but your bladder and bowels would have atrophied to the point of diaper dependence (micro bladder and bowels). Shinki will not get mad if you remove this chip during or after this stage.
- Going to Shinki will result in her diapering you. She says that trying to stay continent is a futile manner and suggests you to use your diapers as a toilet.

## 2.1A It's gone... It's gone...
- MC can't feel her bladder or bowels. Officially incontinent.
- Talking to Eirin will tell you that the chip has merged with your mental psyche and that removal will still leave you incontinent. Removal is now no longer possible
- Shinki will destroy your underwear after this point if it hasn't before.



### Stage 2.3 Messy Magic
- Shinki will give you laxative cookies, causing you to piss and shit the bed.
- Shinki will go to your room, put you in anesthesia, and insert an overactive bladder (and irrital bowel) chip to make you have accidents.

## Stage 3: My Free Daughter Can't Be This Cute
- Transform MC into a female (or futa) toddler

## Stage 4: Total Control
- Make MC unable to resist Shinki, that he or she will feel absent and empty without her.